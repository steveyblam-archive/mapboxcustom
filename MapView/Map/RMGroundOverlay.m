/*
     File: TiledPDFView.m
 Abstract: This view is backed by a CATiledLayer into which the PDF page is rendered into.
  Version: 2
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2012 Apple Inc. All Rights Reserved.
 
 */

#import "RMGroundOverlay.h"
#define TMP NSTemporaryDirectory()


@implementation RMGroundOverlay
{
    CGPDFPageRef pdfPage;
    CGFloat myScale;
    CGRect originalSize;
    CGRect imageRect;
    NSString *pdf;

}

@synthesize cachedImage;
@synthesize location;
@synthesize zoom;
@synthesize width;
@synthesize rotation;


// Create a new TiledPDFView with the desired frame and scale.
- (id) initWithPDF:(NSString *)url width:(CGFloat ) w zoom:(CGFloat) z location:(CLLocationCoordinate2D) l rotation:(CGFloat)r;
{
 
    rotation = r;
    
    location = l;
    
    pdf = url;
    
    zoom = z;
    
    width = w;
    
    NSURL *pdfURL = [[NSBundle mainBundle] URLForResource:pdf withExtension:@"pdf"];
    
    CGPDFDocumentRef PDFDocument = CGPDFDocumentCreateWithURL((__bridge CFURLRef)pdfURL);
    
    pdfPage = CGPDFDocumentGetPage(PDFDocument, 1);
    
    // Determine the size of the PDF page.
    CGRect pageRect = CGPDFPageGetBoxRect(pdfPage, kCGPDFMediaBox);
    
    myScale = w / pageRect.size.width;
    
    pageRect.size = CGSizeMake(pageRect.size.width*myScale, pageRect.size.height*myScale);
    
    self = [super initWithFrame:pageRect];
    
    if (self) {
        
        CATiledLayer *tiledLayer = (CATiledLayer *)[self layer];
        /*
         levelsOfDetail and levelsOfDetailBias determine how the layer is rendered at different zoom levels. This only matters while the view is zooming, because once the the view is done zooming a new TiledPDFView is created at the correct size and scale.
         */
        tiledLayer.levelsOfDetail = 2;
        tiledLayer.levelsOfDetailBias = 3;
        tiledLayer.tileSize = CGSizeMake(1024, 1024);

    }
    
    self.bounds = pageRect;
    
    // need to shrink it down to make it fit and rotate
    //self.tiledLayer.transform = CATransform3DMakeRotation(rotation * M_PI / 180, 0.0f, 0.0f, 1.0f);
    self.transform = CGAffineTransformScale(self.transform, pow(2 , -zoom), pow(2 , -zoom));
    self.transform = CGAffineTransformRotate(self.transform, rotation * M_PI / 180);

    [self setOpaque:NO];
    
    return self;
}


+ (Class)layerClass
{
    return [CATiledLayer class];
}

- (CATiledLayer *)tiledLayer
{
    return (CATiledLayer *)self.layer;
}


/*- (void)drawRect:(CGRect)rect
{
    
    CGContextRef originalContext = UIGraphicsGetCurrentContext();
    
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGFloat scale = CGContextGetCTM(originalContext).a;
    NSInteger col = (CGRectGetMinX(rect) * scale) / 512;
    NSInteger row = (CGRectGetMinY(rect) * scale) / 512;
    NSString *filename = [NSString stringWithFormat:@"%@-%u-%u-%u.jpg" , pdf, (int) scale , (int) row , (int) col];
    
    UIImage *tile = [self getCachedImage:filename];
    
    NSLog(@"rect size %f %f %f %f" , rect.origin.x , rect.origin.y, rect.size.width , rect.size.height);
    
    if(tile == nil) {
        
        CGContextClipToRect(context, rect);
        CGContextScaleCTM(context, scale, scale);
    
        CGContextSetRGBFillColor(context, 1.0,1.0,1.0,0.4);
        CGContextFillRect(context, rect);
    
        // Scale the context so that the PDF page is rendered at the correct size for the zoom level.
        CGContextScaleCTM(context, myScale, myScale);
        CGContextDrawPDFPage(context, pdfPage);
    
        tile = UIGraphicsGetImageFromCurrentImageContext();
        
        if(tile != nil) {
            
            [self cacheImage:filename image:tile];
            
        }
        
        UIGraphicsEndImageContext();
        
    }

    
    CGContextDrawImage(originalContext, CGRectMake(0, 0, tile.size.width, tile.size.height), tile.CGImage);
    
    //[tile drawInRect:rect];
} */


// Draw the CGPDFPageRef into the layer at the correct scale.
-(void)drawLayer:(CALayer*)layer inContext:(CGContextRef)context
{
    
    
    // Fill the background with white.
    CGContextSetRGBFillColor(context, 1.0,1.0,1.0,0.4);
    CGContextFillRect(context, self.bounds);
 
    // Flip the context so that the PDF page is rendered right side up.
    CGContextTranslateCTM(context, 0.0, self.bounds.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);

    // Scale the context so that the PDF page is rendered at the correct size for the zoom level.
    CGContextScaleCTM(context, myScale, myScale);
    CGContextDrawPDFPage(context, pdfPage);

    
} 

- (void) cacheImage:(NSString *)filename image:(UIImage *) image {
    
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSLog(@"saving jpeg");
	NSString *jpegFilePath = [NSString stringWithFormat:@"%@/%@",docDir , filename];
	NSData *cache = [NSData dataWithData:UIImageJPEGRepresentation(image, 1.0f)];//1.0f = 100% quality
	[cache writeToFile:jpegFilePath atomically:YES];
        
    
    
    
}
                      

- (UIImage *) getCachedImage: (NSString *) filename {
    
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *fileLocation = [NSString stringWithFormat:@"%@/%@",docDir , filename];
    
    // Check for a cached version
    if([[NSFileManager defaultManager] fileExistsAtPath: fileLocation])
    {
        NSLog(@"file exists %@" , fileLocation);
    
        // load the tile
        UIImage *image = [UIImage imageWithContentsOfFile:fileLocation];
        
        if(image == nil)
            NSLog(@"uh oh the image didn't load");
        
        return image;
        
    } else {
        NSLog(@"file doesn't exist");
        return nil;
        
    }
            
            
}

- (void) updateWithScale:(CGFloat)newZoom {
    
    
//    CGFloat scale = pow(2, newZoom - zoom);
//    
//    NSLog(@"scale that bitch %f " , scale);
//    
//    // Determine the size of the PDF page.
//    CGRect pageRect = CGPDFPageGetBoxRect(pdfPage, kCGPDFMediaBox);
//    
//    myScale = width * scale / pageRect.size.width;
//    
//    pageRect.size = CGSizeMake(pageRect.size.width*myScale, pageRect.size.height*myScale);
//    
//    self.frame = pageRect;
//    
//    // need to shrink it down to make it fit
//    //self.transform = CGAffineTransformScale(self.transform, 0.00001, 0.00001);
//    //self.transform = CGAffineTransformRotate(self.transform, 0 * M_PI / 180);
//
//    [self.layer setNeedsDisplay];
    
    
}



// Clean up.
- (void)dealloc
{
    CGPDFPageRelease(pdfPage);    
}


@end
